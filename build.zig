const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();

    const pkg = std.build.Pkg{
        .name = "irate",
        .source = .{ .path = "src/irate.zig" },
    };

    {
        const irate_tests = b.addTest("src/irate.zig");
        irate_tests.setBuildMode(mode);

        const step = b.step("test", "Run the test suite");
        step.dependOn(&irate_tests.step);
    }
}
