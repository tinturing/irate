const std = @import("std");
const testing = std.testing;

/// The `Iter` type is a wrapper for iterators. It makes it easy to chain
/// iterators with iterator adapters. An iterator adapter is a function that
/// takes an iterator and returns a (modified) iterator. Examples for this are
/// functions such as `map` or `filter`. By wrapping any iterator with the
/// `Iter` type these iterator adapters can be called as methods.
///
/// Many generally useful iterator adapters are already included. Iterator
/// adapters that are not included in `Iter` can still be used via the
/// `Iter.then` method.
///
/// A value of type `Iter` (e.g. `Iter(MyIter)`) is itself an iterator, because
/// it has a `next` method. Alternatively, the contained iterator (`MyIter`)
/// can be accessed directly via the field `iter`.
///
/// Example:
///
///     fn even(x: i32) bool { return @rem(x, 2) == 0; }
///     fn double(x: i32) i32 { return x * 2; }
///
///     var it = iter(range(0, 10))
///         .filter(even)
///         .map(double)
///         .iter;
///
///     // `it` now yields: 0, 4, 8, 12, 16.
///
/// Note: The `.iter` in the last line is optional, since an `Iter` can itself
/// also act as an iterator. Unwrapping an `Iter` with `.iter` can be useful,
/// e.g. to simplify a function's return type.
///
/// The equivalent with conventional nested function calls would be:
///
///     var iter = map(filter(range(0, 10), even), double);
///
pub fn Iter(comptime I: type) type {
    return struct {
        iter: I,

        const Self = @This();

        pub fn init(iter: I) Self {
            return Self{ .iter = iter };
        }

        pub fn next(self: *Self) ?Yield(I) {
            return self.iter.next();
        }

        fn Then(getIterFn: anytype, args: anytype) type {
            return @TypeOf(@call(
                .{ .modifier = .compile_time },
                getIterFn,
                .{@as(I, undefined)} ++ @as(@TypeOf(args), undefined),
            ));
        }

        /// Produces an `Iter` by applying `getIterFn` to the contained iterator
        /// (`Iter.iter`). It allows an `Iter` to be used with iterators that
        /// are not natively supported.
        ///
        /// `getIterFn` must...
        /// ...take an iterator as its first argument,
        /// ...and return an iterator.
        /// The other arguments must be passed via the anonymous struct `args`.
        ///
        /// Example:
        ///
        /// Imagine `Iter.filter` and `Iter.map` did not exist. We could use
        /// `Iter.then` and the free functions `filter` and `map` as follows:
        ///
        ///     var iter = iter(range(0, 10))
        ///         .then(filter, .{even})
        ///         .then(map, .{double});
        ///
        pub fn then(self: Self, getIterFn: anytype, args: anytype) Iter(Then(getIterFn, args)) {
            return Iter(Then(getIterFn, args)).init(@call(.{}, getIterFn, .{self.iter} ++ args));
        }

        pub fn filter(self: Self, predicate: *const fn (Yield(I)) bool) Iter(Filter(I)) {
            return Iter(Filter(I)).init(Filter(I).init(self.iter, predicate));
        }

        pub fn mapT(self: Self, comptime R: type, mapFn: *const fn (Yield(I)) R) Iter(Map(I, R)) {
            return Iter(Map(I, R)).init(Map(I, R).init(self.iter, mapFn));
        }

        pub fn map(self: Self, mapFn: anytype) Iter(Map(I, Return(@TypeOf(mapFn)))) {
            return self.mapT(Return(@TypeOf(mapFn)), mapFn);
        }

        pub fn cycle(self: Self) Iter(Cycle(I)) {
            return Iter(Cycle(I)).init(Cycle(I).init(self.iter));
        }

        pub fn enumerate(self: Self) Iter(Enumerate(I)) {
            return Iter(Enumerate(I)).init(Enumerate(I).init(self.iter));
        }

        pub fn flatten(self: Self) Iter(Flatten(I)) {
            return Iter(Flatten(I)).init(Flatten(I).init(self.iter));
        }

        pub fn peekable(self: Self) Iter(Peekable(I)) {
            return Iter(Peekable(I)).init(Peekable(I).init(self.iter));
        }

        pub fn take(self: Self, n: usize) Iter(Take(I)) {
            return Iter(Take(I)).init(Take(I).init(self.iter, n));
        }

        pub fn skip(self: Self, n: usize) Iter(Skip(I)) {
            return Iter(Skip(I)).init(Skip(I).init(self.iter, n));
        }

        pub fn takeWhile(self: Self, predicate: *const fn (Yield(I)) bool) Iter(TakeWhile(I)) {
            return Iter(TakeWhile(I)).init(TakeWhile(I).init(self.iter, predicate));
        }

        pub fn skipWhile(self: Self, predicate: *const fn (Yield(I)) bool) Iter(SkipWhile(I)) {
            return Iter(SkipWhile(I)).init(SkipWhile(I).init(self.iter, predicate));
        }

        pub fn zipT(self: Self, comptime O: type, other: O) Iter(Zip(I, O)) {
            return Iter(Zip(I, O)).init(Zip(I, O).init(self.iter, other));
        }

        pub fn zip(self: Self, other: anytype) Iter(Zip(I, @TypeOf(other))) {
            return self.zipT(@TypeOf(other), other);
        }

        pub fn chainT(self: Self, comptime O: type, other: O) Iter(Chain(I, O)) {
            return Iter(Chain(I, O)).init(Chain(I, O).init(self.iter, other));
        }

        pub fn chain(self: Self, other: anytype) Iter(Chain(I, @TypeOf(other))) {
            return self.chainT(@TypeOf(other), other);
        }

        pub fn foldT(self: Self, comptime T: type, foldFn: *const fn (T, Yield(I)) T, start: T) T {
            return Fold(I, T).fold(self.iter, start, foldFn);
        }

        pub fn fold(
            self: Self,
            start: anytype,
            foldFn: *const fn (@TypeOf(start), Yield(I)) @TypeOf(start),
        ) @TypeOf(start) {
            return self.foldT(@TypeOf(start), foldFn, start);
        }

        pub fn collect(self: Self, allocator: std.mem.Allocator) !std.ArrayList(Yield(I)) {
            return Collect(I).collect(allocator, self.iter);
        }
    };
}

pub fn toIter(iter: anytype) Iter(@TypeOf(iter)) {
    return Iter(@TypeOf(iter)).init(iter);
}

pub usingnamespace struct {
    pub const iter = toIter;
};

pub fn Yield(comptime I: type) type {
    const msg = "type '" ++ @typeName(I) ++ "' is not an iterator";

    if (!std.meta.trait.hasFn("next")(I)) {
        @compileError(msg ++ ": it needs a function called 'next'");
    }

    const info = @typeInfo(@TypeOf(I.next)).Fn;

    if (info.args.len != 1 or info.args[0].arg_type != *I) {
        @compileError(msg ++ ": 'fn next' needs a parameter of type '" ++
            @typeName(*I) ++ "'");
    }

    if (info.return_type) |NextReturnType| {
        return switch (@typeInfo(NextReturnType)) {
            .Optional => |opt| opt.child,
            else => @compileError(msg ++
                ": expected 'fn next' to return an optional, found '" ++
                @typeName(NextReturnType) ++ "'"),
        };
    } else {
        @compileError(msg ++ ": 'fn next' has no return type");
    }
}

test "Yield" {
    const TestIterator = struct {
        pub fn next(self: *@This()) ?i17 {
            _ = self;
            return null;
        }
    };

    try testing.expectEqual(i17, Yield(TestIterator));
}

const SliceModifier = enum { value, ptr, ptr_const };

pub fn SliceBase(comptime T: type, comptime modifier: SliceModifier) type {
    const SliceType = switch (modifier) {
        .value, .ptr_const => []const T,
        .ptr => []T,
    };

    const ValueType = switch (modifier) {
        .value => T,
        .ptr => *T,
        .ptr_const => *const T,
    };

    return struct {
        slice: SliceType,

        const Self = @This();

        pub fn init(slice: SliceType) Self {
            return Self{ .slice = slice };
        }

        pub fn next(self: *Self) ?ValueType {
            if (self.slice.len == 0) {
                return null;
            }

            defer self.slice = self.slice[1..];
            return switch (modifier) {
                .value => self.slice[0],
                .ptr, .ptr_const => &self.slice[0],
            };
        }
    };
}

pub fn Slice(comptime T: type) type {
    return SliceBase(T, .value);
}

test "Slice.next" {
    var numbers = [_]i32{ 42, 17, 99 };
    var iter = Slice(i32).init(numbers[0..]);

    try testing.expectEqual(@as(?i32, 42), iter.next());
    try testing.expectEqual(@as(?i32, 17), iter.next());
    try testing.expectEqual(@as(?i32, 99), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn SlicePtr(comptime T: type) type {
    return SliceBase(T, .ptr);
}

test "SlicePtr.next" {
    var numbers = [_]i32{ 42, 17, 99 };
    var iter = SlicePtr(i32).init(numbers[0..]);

    try testing.expectEqual(@as(i32, 42), iter.next().?.*);

    const ptr = iter.next().?;
    try testing.expectEqual(@as(i32, 17), ptr.*);
    ptr.* = 18;

    try testing.expectEqual(@as(i32, 99), iter.next().?.*);
    try testing.expectEqual(@as(?*i32, null), iter.next());

    try testing.expectEqualSlices(i32, &[_]i32{ 42, 18, 99 }, numbers[0..]);
}

pub fn SlicePtrConst(comptime T: type) type {
    return SliceBase(T, .ptr_const);
}

test "SlicePtrConst.next" {
    const numbers = [_]i32{ 42, 17, 99 };
    var iter = SlicePtrConst(i32).init(numbers[0..]);

    try testing.expectEqual(@as(i32, 42), iter.next().?.*);
    try testing.expectEqual(@as(i32, 17), iter.next().?.*);
    try testing.expectEqual(@as(i32, 99), iter.next().?.*);
    try testing.expectEqual(@as(?*const i32, null), iter.next());
}

const ChunksModifier = enum { mutable, constant };

fn ChunksBase(comptime T: type, comptime modifier: ChunksModifier) type {
    const SliceType = switch (modifier) {
        .mutable => []T,
        .constant => []const T,
    };

    return struct {
        slice: SliceType,
        chunk_size: usize,

        const Self = @This();

        pub fn init(slice: SliceType, chunk_size: usize) Self {
            return Self{
                .slice = slice,
                .chunk_size = chunk_size,
            };
        }

        pub fn next(self: *Self) ?SliceType {
            if (self.slice.len < self.chunk_size) {
                return null;
            } else {
                defer self.slice = self.slice[self.chunk_size..];
                return self.slice[0..self.chunk_size];
            }
        }
    };
}

pub fn Chunks(comptime T: type) type {
    return ChunksBase(T, .mutable);
}

test "Chunks.next" {
    var numbers = [_]i32{ 42, 17, 99, 0, 100 };
    var iter = Chunks(i32).init(numbers[0..], 2);

    // NOTE The last value in the array (`0`) is omitted.
    try testing.expectEqualSlices(i32, &[_]i32{ 42, 17 }, iter.next().?);
    const slice = iter.next().?;
    try testing.expectEqualSlices(i32, &[_]i32{ 99, 0 }, slice);
    slice[1] = 1;
    try testing.expectEqual(@as(?[]i32, null), iter.next());

    try testing.expectEqualSlices(i32, &[_]i32{ 42, 17, 99, 1, 100 }, numbers[0..]);
}

pub fn ChunksConst(comptime T: type) type {
    return ChunksBase(T, .constant);
}

test "ChunksConst.next" {
    const numbers = [_]i32{ 42, 17, 99, 0, 100 };
    var iter = ChunksConst(i32).init(numbers[0..], 2);

    // NOTE The last value in the array (`0`) is omitted.
    try testing.expectEqualSlices(i32, &[_]i32{ 42, 17 }, iter.next().?);
    try testing.expectEqualSlices(i32, &[_]i32{ 99, 0 }, iter.next().?);
    try testing.expectEqual(@as(?[]const i32, null), iter.next());
}

pub fn Windows(comptime T: type) type {
    return struct {
        slice: []T,
        win_size: usize,

        const Self = @This();

        pub const Error = error{WindowSizeZero};

        pub fn init(slice: []T, win_size: usize) Error!Self {
            if (win_size == 0) {
                return Error.WindowSizeZero;
            }

            return Self{
                .slice = slice,
                .win_size = win_size,
            };
        }

        pub fn next(self: *Self) ?[]T {
            if (self.slice.len < self.win_size) {
                return null;
            } else {
                defer self.slice = self.slice[1..];
                return self.slice[0..self.win_size];
            }
        }
    };
}

test "Windows.next" {
    var numbers = [_]i32{ 42, 17, 99, 0, 100 };
    var iter = try Windows(i32).init(numbers[0..], 3);

    try testing.expectEqualSlices(i32, &[_]i32{ 42, 17, 99 }, iter.next().?);
    try testing.expectEqualSlices(i32, &[_]i32{ 17, 99, 0 }, iter.next().?);
    try testing.expectEqualSlices(i32, &[_]i32{ 99, 0, 100 }, iter.next().?);
    try testing.expectEqual(@as(?[]i32, null), iter.next());
}

test "Windows.init, win_size == 0" {
    var result = Windows(i32).init(&[_]i32{}, 0);

    try testing.expectError(Windows(i32).Error.WindowSizeZero, result);
}

pub fn Range(comptime T: type) type {
    return struct {
        i: T,
        end: T,

        const Self = @This();

        pub fn init(start: T, end: T) Self {
            return Self{ .i = start, .end = end };
        }

        pub fn next(self: *Self) ?T {
            if (self.i >= self.end) {
                return null;
            }

            defer self.i += 1;
            return self.i;
        }
    };
}

pub fn range(start: anytype, end: @TypeOf(start)) Range(@TypeOf(start)) {
    return Range(@TypeOf(start)).init(start, end);
}

test "Range.next" {
    var iter = range(@as(i32, 0), 3);

    try testing.expectEqual(@as(?i32, 0), iter.next());
    try testing.expectEqual(@as(?i32, 1), iter.next());
    try testing.expectEqual(@as(?i32, 2), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn Repeat(comptime T: type) type {
    return struct {
        item: T,

        const Self = @This();

        pub fn init(item: T) Self {
            return Self{ .item = item };
        }

        pub fn next(self: *Self) ?T {
            return self.item;
        }
    };
}

pub fn repeat(item: anytype) Repeat(@TypeOf(item)) {
    return Repeat(@TypeOf(item)).init(item);
}

test "Repeat.next" {
    var iter = repeat(@as(i32, 42));

    try testing.expectEqual(@as(?i32, 42), iter.next());
    try testing.expectEqual(@as(?i32, 42), iter.next());
    try testing.expectEqual(@as(?i32, 42), iter.next());
    try testing.expectEqual(@as(?i32, 42), iter.next());
    try testing.expectEqual(@as(?i32, 42), iter.next());

    // NOTE Since the iterator repeats the value indefinitely,
    // `fn next` will never return `null`, but there is no
    // good way to prove this in a `test`.
}

pub fn Once(comptime T: type) type {
    return struct {
        item: ?T,

        const Self = @This();

        fn init(item: T) Self {
            return Self{ .item = item };
        }

        fn next(self: *Self) ?T {
            defer self.item = null;
            return self.item;
        }
    };
}

pub fn once(item: anytype) Once(@TypeOf(item)) {
    return Once(@TypeOf(item)).init(item);
}

test "Once.next" {
    var iter = once(@as(i32, 42));

    try testing.expectEqual(@as(?i32, 42), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn Empty(comptime T: type) type {
    return struct {
        const Self = @This();

        fn next(self: *Self) ?T {
            _ = self;
            return null;
        }
    };
}

test "Empty.next" {
    var iter = Empty(i32){};

    try testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn Cycle(comptime I: type) type {
    return struct {
        iter: I,
        orig: I,

        const Self = @This();

        fn init(iter: I) Self {
            return Self{ .iter = iter, .orig = iter };
        }

        fn next(self: *Self) ?Yield(I) {
            if (self.iter.next()) |current| {
                return current;
            } else {
                self.iter = self.orig;
                return self.iter.next();
            }
        }
    };
}

pub fn cycle(iter: anytype) Cycle(@TypeOf(iter)) {
    return Cycle(@TypeOf(iter)).init(iter);
}

test "Cycle.next" {
    var numbers = [_]i32{ 42, 17, 99 };
    var iter = cycle(Slice(i32).init(numbers[0..]));

    try testing.expectEqual(@as(?i32, 42), iter.next());
    try testing.expectEqual(@as(?i32, 17), iter.next());
    try testing.expectEqual(@as(?i32, 99), iter.next());
    try testing.expectEqual(@as(?i32, 42), iter.next());
    try testing.expectEqual(@as(?i32, 17), iter.next());
    try testing.expectEqual(@as(?i32, 99), iter.next());
    try testing.expectEqual(@as(?i32, 42), iter.next());

    // NOTE Since the iterator cycles the value indefinitely,
    // `fn next` will never return `null`, but there is no
    // good way to prove this in a `test`.
}

pub fn Filter(comptime I: type) type {
    return struct {
        iter: I,
        predicate: *const fn (Yield(I)) bool,

        const Self = @This();

        pub fn init(iter: I, predicate: *const fn (Yield(I)) bool) Self {
            return Self{ .iter = iter, .predicate = predicate };
        }

        pub fn next(self: *Self) ?Yield(I) {
            return while (self.iter.next()) |current| {
                if (self.predicate(current)) {
                    break current;
                }
            } else null;
        }
    };
}

pub fn filter(
    iter: anytype,
    predicate: *const fn (Yield(@TypeOf(iter))) bool,
) Filter(@TypeOf(iter)) {
    return Filter(@TypeOf(iter)).init(iter, predicate);
}

test "Filter.next" {
    const even = struct {
        fn impl(x: i32) bool {
            return @rem(x, 2) == 0;
        }
    };

    var numbers = [_]i32{ 0, 1, 2, 3, 4, 5 };
    var iter = filter(Slice(i32).init(numbers[0..]), even.impl);

    try testing.expectEqual(@as(?i32, 0), iter.next());
    try testing.expectEqual(@as(?i32, 2), iter.next());
    try testing.expectEqual(@as(?i32, 4), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn Map(comptime I: type, comptime R: type) type {
    return struct {
        iter: I,
        mapFn: *const fn (Yield(I)) R,

        const Self = @This();

        pub fn init(iter: I, mapFn: *const fn (Yield(I)) R) Self {
            return Self{ .iter = iter, .mapFn = mapFn };
        }

        pub fn next(self: *Self) ?R {
            return if (self.iter.next()) |current|
                self.mapFn(current)
            else
                null;
        }
    };
}

pub fn Return(comptime T: type) type {
    const msg = "type '" ++ @typeName(T) ++ "'";

    const info = switch (@typeInfo(T)) {
        .Fn => |f| f,
        else => @compileError(msg ++ " is not a function"),
    };

    return if (info.return_type) |return_type|
        return_type
    else
        @compileError(msg ++ " has no return type");
}

pub fn map(iter: anytype, mapFn: anytype) Map(@TypeOf(iter), Return(@TypeOf(mapFn))) {
    return Map(@TypeOf(iter), Return(@TypeOf(mapFn))).init(iter, mapFn);
}

test "Map.next" {
    const incr = struct {
        fn impl(x: i31) i32 {
            return x + 1;
        }
    };

    var numbers = [_]i31{ 0, 1, 2 };
    var iter = map(Slice(i31).init(numbers[0..]), incr.impl);

    try testing.expectEqual(@as(?i32, 1), iter.next());
    try testing.expectEqual(@as(?i32, 2), iter.next());
    try testing.expectEqual(@as(?i32, 3), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn Flatten(comptime I: type) type {
    return struct {
        iter: I,
        inner: ?Yield(I) = null,

        const Self = @This();

        pub fn init(iter: I) Self {
            return Self{ .iter = iter };
        }

        pub fn next(self: *Self) ?Yield(Yield(I)) {
            while (true) {
                if (self.inner) |*inner| {
                    if (inner.next()) |current| {
                        return current;
                    } else {
                        self.inner = null;
                    }
                }

                if (self.iter.next()) |inner| {
                    self.inner = inner;
                } else {
                    return null;
                }
            }
        }
    };
}

pub fn flatten(iter: anytype) Flatten(@TypeOf(iter)) {
    return Flatten(@TypeOf(iter)).init(iter);
}

test "Flatten.next" {
    const init = Range(i32).init;
    var iters = [_]Range(i32){ init(0, 2), init(0, 3), init(0, 1) };
    var iter = flatten(Slice(Range(i32)).init(iters[0..]));

    try testing.expectEqual(@as(?i32, 0), iter.next());
    try testing.expectEqual(@as(?i32, 1), iter.next());
    try testing.expectEqual(@as(?i32, 0), iter.next());
    try testing.expectEqual(@as(?i32, 1), iter.next());
    try testing.expectEqual(@as(?i32, 2), iter.next());
    try testing.expectEqual(@as(?i32, 0), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

// REFACTOR pending #4335
pub fn Indexed(comptime T: type) type {
    return Pair(usize, T);
}

pub fn Enumerate(comptime I: type) type {
    return struct {
        iter: I,
        count: usize = 0,

        const Self = @This();

        pub fn init(iter: I) Self {
            return Self{ .iter = iter };
        }

        pub fn next(self: *Self) ?Indexed(Yield(I)) {
            if (self.iter.next()) |current| {
                defer self.count += 1;
                return Indexed(Yield(I)){ self.count, current };
            } else {
                return null;
            }
        }
    };
}

pub fn enumerate(iter: anytype) Enumerate(@TypeOf(iter)) {
    return Enumerate(@TypeOf(iter)).init(iter);
}

test "Enumerate.next" {
    var numbers = [_]i32{ 42, 17, 99 };
    var iter = enumerate(Slice(i32).init(numbers[0..]));

    try testing.expectEqual(@as(?Indexed(i32), Indexed(i32){ 0, 42 }), iter.next());
    try testing.expectEqual(@as(?Indexed(i32), Indexed(i32){ 1, 17 }), iter.next());
    try testing.expectEqual(@as(?Indexed(i32), Indexed(i32){ 2, 99 }), iter.next());
    try testing.expectEqual(@as(?Indexed(i32), null), iter.next());
}

pub fn Peekable(comptime I: type) type {
    return struct {
        iter: I,
        peeked: ??Yield(I) = null,

        const Self = @This();

        pub fn init(iter: I) Self {
            return Self{ .iter = iter };
        }

        pub fn next(self: *Self) ?Yield(I) {
            if (self.peeked) |peeked| {
                self.peeked = null;
                return peeked;
            } else {
                return self.iter.next();
            }
        }

        pub fn peek(self: *Self) ?Yield(I) {
            if (self.peeked) |peeked| {
                return peeked;
            } else if (self.iter.next()) |peeked| {
                self.peeked = peeked;
                return peeked;
            }
            return null;
        }
    };
}

pub fn peekable(iter: anytype) Peekable(@TypeOf(iter)) {
    return Peekable(@TypeOf(iter)).init(iter);
}

test "Peekable.next" {
    var numbers = [_]i32{ 42, 17, 99 };
    var iter = peekable(Slice(i32).init(numbers[0..]));

    try testing.expectEqual(@as(?i32, 42), iter.peek());
    try testing.expectEqual(@as(?i32, 42), iter.peek());
    try testing.expectEqual(@as(?i32, 42), iter.next());
    try testing.expectEqual(@as(?i32, 17), iter.next());
    try testing.expectEqual(@as(?i32, 99), iter.peek());
    try testing.expectEqual(@as(?i32, 99), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn Take(comptime I: type) type {
    return struct {
        iter: I,
        n: usize,

        const Self = @This();

        pub fn init(iter: I, n: usize) Self {
            return Self{ .iter = iter, .n = n };
        }

        pub fn next(self: *Self) ?Yield(I) {
            if (self.n > 0) {
                self.n -= 1;
                return self.iter.next();
            } else {
                return null;
            }
        }
    };
}

pub fn take(iter: anytype, n: usize) Take(@TypeOf(iter)) {
    return Take(@TypeOf(iter)).init(iter, n);
}

test "Take.next" {
    var iter = take(Range(i32).init(0, 1000), 3);

    try testing.expectEqual(@as(?i32, 0), iter.next());
    try testing.expectEqual(@as(?i32, 1), iter.next());
    try testing.expectEqual(@as(?i32, 2), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn Skip(comptime I: type) type {
    return struct {
        iter: I,
        n: usize,

        const Self = @This();

        pub fn init(iter: I, n: usize) Self {
            return Self{ .iter = iter, .n = n };
        }

        pub fn next(self: *Self) ?Yield(I) {
            while (self.n > 0) : (self.n -= 1) {
                _ = self.iter.next() orelse break;
            }

            return self.iter.next();
        }
    };
}

pub fn skip(iter: anytype, n: usize) Skip(@TypeOf(iter)) {
    return Skip(@TypeOf(iter)).init(iter, n);
}

test "Skip.next" {
    var iter = skip(Range(i32).init(0, 10), 7);

    try testing.expectEqual(@as(?i32, 7), iter.next());
    try testing.expectEqual(@as(?i32, 8), iter.next());
    try testing.expectEqual(@as(?i32, 9), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn TakeWhile(comptime I: type) type {
    return struct {
        iter: I,
        done: bool = false,
        predicate: *const fn (Yield(I)) bool,

        const Self = @This();

        pub fn init(iter: I, predicate: *const fn (Yield(I)) bool) Self {
            return Self{ .iter = iter, .predicate = predicate };
        }

        pub fn next(self: *Self) ?Yield(I) {
            if (!self.done) {
                if (self.iter.next()) |current| {
                    if (self.predicate(current)) {
                        return current;
                    } else {
                        self.done = true;
                    }
                }
            }

            return null;
        }
    };
}

pub fn takeWhile(
    iter: anytype,
    predicate: *const fn (Yield(@TypeOf(iter))) bool,
) TakeWhile(@TypeOf(iter)) {
    return TakeWhile(@TypeOf(iter)).init(iter, predicate);
}

test "TakeWhile.next" {
    const small = struct {
        pub fn impl(x: i32) bool {
            return x < 3;
        }
    };

    var iter = takeWhile(Range(i32).init(0, 1000), small.impl);

    try testing.expectEqual(@as(?i32, 0), iter.next());
    try testing.expectEqual(@as(?i32, 1), iter.next());
    try testing.expectEqual(@as(?i32, 2), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn SkipWhile(comptime I: type) type {
    return struct {
        iter: I,
        done: bool = false,
        predicate: *const fn (Yield(I)) bool,

        const Self = @This();

        pub fn init(iter: I, predicate: *const fn (Yield(I)) bool) Self {
            return Self{ .iter = iter, .predicate = predicate };
        }

        pub fn next(self: *Self) ?Yield(I) {
            if (!self.done) {
                self.done = true;

                while (self.iter.next()) |current| {
                    if (!self.predicate(current)) {
                        return current;
                    }
                }
            }

            return self.iter.next();
        }
    };
}

pub fn skipWhile(
    iter: anytype,
    predicate: *const fn (Yield(@TypeOf(iter))) bool,
) SkipWhile(@TypeOf(iter)) {
    return SkipWhile(@TypeOf(iter)).init(iter, predicate);
}

test "SkipWhile.next" {
    const small = struct {
        pub fn impl(x: i32) bool {
            return x < 3;
        }
    };

    var iter = skipWhile(Range(i32).init(0, 6), small.impl);

    try testing.expectEqual(@as(?i32, 3), iter.next());
    try testing.expectEqual(@as(?i32, 4), iter.next());
    try testing.expectEqual(@as(?i32, 5), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

// REFACTOR pending #4335
pub fn Pair(comptime T1: type, comptime T2: type) type {
    return std.meta.Tuple(&.{ T1, T2 });
}

pub fn Zip(comptime I1: type, comptime I2: type) type {
    return struct {
        fst: I1,
        snd: I2,

        const Self = @This();

        pub fn init(fst: I1, snd: I2) Self {
            return Self{ .fst = fst, .snd = snd };
        }

        pub fn next(self: *Self) ?Pair(Yield(I1), Yield(I2)) {
            if (self.fst.next()) |t1| {
                if (self.snd.next()) |t2| {
                    return Pair(Yield(I1), Yield(I2)){ t1, t2 };
                }
            }

            return null;
        }
    };
}

pub fn zip(fst: anytype, snd: anytype) Zip(@TypeOf(fst), @TypeOf(snd)) {
    return Zip(@TypeOf(fst), @TypeOf(snd)).init(fst, snd);
}

test "Zip.next" {
    var numbers = [_]i32{ 42, 17, 99 };
    var iter = zip(Slice(i32).init(numbers[0..]), Range(u32).init(0, 1000));

    try testing.expectEqual(@as(?Pair(i32, u32), Pair(i32, u32){ 42, 0 }), iter.next());
    try testing.expectEqual(@as(?Pair(i32, u32), Pair(i32, u32){ 17, 1 }), iter.next());
    try testing.expectEqual(@as(?Pair(i32, u32), Pair(i32, u32){ 99, 2 }), iter.next());
    try testing.expectEqual(@as(?Pair(i32, u32), null), iter.next());
}

pub fn Chain(comptime I1: type, comptime I2: type) type {
    if (Yield(I1) != Yield(I2)) {
        const msg = std.fmt.comptimePrint(
            "iterators '{any}' and '{any}' do not yield the same type",
            .{ I1, I2 },
        );
        @compileError(msg);
    }

    return struct {
        fst: I1,
        snd: I2,

        const Self = @This();

        pub fn init(fst: I1, snd: I2) Self {
            return Self{ .fst = fst, .snd = snd };
        }

        pub fn next(self: *Self) ?Yield(I1) {
            return self.fst.next() orelse self.snd.next();
        }
    };
}

pub fn chain(fst: anytype, snd: anytype) Chain(@TypeOf(fst), @TypeOf(snd)) {
    return Chain(@TypeOf(fst), @TypeOf(snd)).init(fst, snd);
}

test "Chain.next" {
    var numbers = [_]i32{ 42, 17, 99 };
    var iter = chain(Slice(i32).init(numbers[0..]), Range(i32).init(0, 3));

    try testing.expectEqual(@as(?i32, 42), iter.next());
    try testing.expectEqual(@as(?i32, 17), iter.next());
    try testing.expectEqual(@as(?i32, 99), iter.next());
    try testing.expectEqual(@as(?i32, 0), iter.next());
    try testing.expectEqual(@as(?i32, 1), iter.next());
    try testing.expectEqual(@as(?i32, 2), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn Fold(comptime I: type, comptime T: type) type {
    return struct {
        pub fn fold(
            iter: I,
            init: T,
            f: *const fn (T, Yield(I)) T,
        ) T {
            var acc = init;

            var iter_mut = iter;
            while (iter_mut.next()) |current| {
                acc = f(acc, current);
            }

            return acc;
        }
    };
}

pub fn fold(
    iter: anytype,
    init: anytype,
    f: *const fn (@TypeOf(init), Yield(@TypeOf(iter))) @TypeOf(init),
) @TypeOf(init) {
    return Fold(@TypeOf(iter), @TypeOf(init)).fold(iter, init, f);
}

test "Fold.fold" {
    const add = struct {
        fn impl(acc: i32, curr: i32) i32 {
            return acc + curr;
        }
    };

    var iter = Range(i32).init(1, 7);
    var result = fold(iter, @as(i32, 0), add.impl);

    try testing.expectEqual(@as(i32, 21), result);

    // NOTE `fold` copies the iterator, so the original still yields its values.
    try testing.expectEqual(@as(?i32, 1), iter.next());
    try testing.expectEqual(@as(?i32, 2), iter.next());
    try testing.expectEqual(@as(?i32, 3), iter.next());
    try testing.expectEqual(@as(?i32, 4), iter.next());
    try testing.expectEqual(@as(?i32, 5), iter.next());
    try testing.expectEqual(@as(?i32, 6), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}

pub fn Collect(comptime I: type) type {
    return struct {
        pub fn collect(allocator: std.mem.Allocator, iter: I) !std.ArrayList(Yield(I)) {
            var list = std.ArrayList(Yield(I)).init(allocator);

            var iter_mut = iter;
            while (iter_mut.next()) |current| {
                try list.append(current);
            }

            return list;
        }
    };
}

pub fn collect(
    allocator: std.mem.Allocator,
    iter: anytype,
) !std.ArrayList(Yield(@TypeOf(iter))) {
    return Collect(@TypeOf(iter)).collect(allocator, iter);
}

test "collect" {
    var numbers = [_]i32{ 42, 17, 99 };
    var iter = Slice(i32).init(numbers[0..]);

    var list = try collect(testing.allocator, iter);
    defer list.deinit();

    try testing.expectEqualSlices(i32, &.{ 42, 17, 99 }, list.items);

    // NOTE `collect` copies the iterator, so the original still yields its values.
    try testing.expectEqual(@as(?i32, 42), iter.next());
    try testing.expectEqual(@as(?i32, 17), iter.next());
    try testing.expectEqual(@as(?i32, 99), iter.next());
    try testing.expectEqual(@as(?i32, null), iter.next());
}
